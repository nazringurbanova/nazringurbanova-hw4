import java.util.*;
public class Main {

    public static void main(String[] args) {
	// write your code here
    Pet myFirstPet = new Pet("cat","Garfield",6,43,new String[]{"eating","sleeping","fighting"});
    Human mother = new Human("Sona","Mammadova",1982);
    Human father = new Human("Aqil","Mammadov",1982);
    Human daughter = new Human("Malak","Mammadova",2010,131,myFirstPet,mother,father,new String[][]{{"Monday","Piano"}});
    Human son = new Human("Farid","Mammadov",2014,121,myFirstPet,mother,father,new String[][]{{"Tuesday","Fottball"}});
    daughter.pet = myFirstPet;
    son.pet = myFirstPet;
    myFirstPet.eat();
    myFirstPet.respond();
    myFirstPet.foul();
    daughter.greetPet();
    daughter.describePet();
        System.out.println(myFirstPet);
        System.out.println(mother);
    }

}
